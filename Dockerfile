FROM alpine:latest

RUN apk --no-cache add curl

COPY cloudflare-ddns-update.sh /

# --chmod=644 
COPY crontab /etc/cron.d/cloudflare-crontab
RUN crontab /etc/cron.d/cloudflare-crontab

ENTRYPOINT [ "crond", "-f", "-l", "2"]
