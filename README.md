## Dynamic DNS using Cloudflare API

Use Cloudflare trace to get your own external ip: https://www.cloudflare.com/cdn-cgi/trace

Use the Cloudflare REST API to update the DNS record: https://api.cloudflare.com/#dns-records-for-a-zone-update-dns-record
